;;;_ fileset-whole.el --- Commands and data applying to filesets as a whole

;;;_. Headers
;;;_ , License
;; Copyright (C) 2011  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: convenience

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; 


;;;_ , Requires

(require 'filesets)

;;;_. Body
;;;_ , Customizations
;;;_  . fileset-whole-alist
(defcustom fileset-whole-alist
   '() 
   "Alist associating fileset names to keyed data"
   :group 'filesets
   :type '(repeat
	     (cons :tag "Fileset"
		(string :tag "Name" :value "")
		(repeat :tag "Data" 
		   (list
		      symbol
		      sexp)))))
(put 'fileset-whole-alist 'risky-local-variable t)

;;;_  . fileset-whole-commands
(defcustom fileset-whole-commands 
   '(
       ("git" 
	  magit-status (default-directory))
       ("eshell" 
	  eshell nil)
       ("Build" 
	  fileset-whole-apply-compile
	  ("make -C" default-directory))
       ("test" 
	  emt:fileset
	  (fileset-name))
       ("retest" 
	  emt:fileset-all
	  (fileset-name))
       ("find-file"
	  (lambda (name)
	     (find-file
		(fileset-whole-pick-file name)))
	  (fileset-name)))

   "List of commands that apply to whole filesets.

Before the command is applied, each arg is evalled with the
following bindings:

  `fileset' - the fileset.
  `fileset-name' - the name of the fileset
  `fileset-whole' - other data associated with the fileset, if any.
  `default-directory' the root directory of the project, if any.

Then the command is applied to the list of args.

It is the user's resposibility to ensure that commands and
functions called in arguments are available; consider using
autoloads."

   :group 'filesets
   ;;$$IMPROVE ME Add a hotkey and put it into some keymap
   ;;$$IMPROVE ME Add a field saying how to find the buffers it
   ;;creates, given its return value.
   :type '(repeat :tag "Commands"
	     (list :tag "Definition" :value ("")
		(string "Name")
		(choice :tag "Command"
		   (function :tag "Function"))
		(repeat :tag "Argument List"
		   (choice :tag "Arguments"
		      ;;$$IMPROVE ME List the bound symbols as options
		      (sexp :tag "Sexp"
			 :value nil))))))

(put 'fileset-whole-commands 'risky-local-variable t)
;;;_ , Variables
;;For now, we don't contemplate multiple filesets applying to one
;;buffer.  Could treat a list of them differently.
(defvar fileset-whole-name-here nil 
   "The fileset that contains this buffer, if any." )
(make-variable-buffer-local 'fileset-whole-name-here)
(put 'fileset-whole-name-here 'permanent-local t)
(put 'fileset-whole-alist 'risky-local-variable nil)
;;;_ , Setting up menus
;;;_  . fileset-whole-setup-menu
(defun fileset-whole-setup-menu ()
   "Add the commands in fileset-whole-commands to fileset menu."
   ;;When is this triggered?
   ;;And see `filesets-get-cmd-menu'.  Either add to it or add another
   ;;menu. 

   (easy-menu-add-item 
      nil ;;Should this be `filesets-menu-in-menu'?
      (append filesets-menu-path (list filesets-menu-name "# Filesets"))
      ["Edit Filesets' Data" fileset-whole-edit]
      "Save Filesets"))

;;;_  . fileset-whole-cmds-to-menu
(defun fileset-whole-cmds-to-menu ()
  "Add fileset-whole commands to filesets command menu."
   (mapcar
      #'(lambda (this)
	   (let
	      ((name (car this)))
	      (easy-menu-add-item 
		 nil ;;Should this be `filesets-menu-in-menu'?
		 (append filesets-menu-path (list filesets-menu-name "+ Commands"))
		 `[,name (fileset-whole-run-cmd 
			    ,name
			    (fileset-whole-read-fileset))])))
      fileset-whole-commands)
   t)

;;;_  . fileset-whole-add-buffer

;;Add a "created" buffer such as gdb, eshell, or magit make to the
;;menus, in a section paralleling the list of files.

;;;_  . fileset-whole-setup-files
;;;###autoload
(defun fileset-whole-setup-files ()
   "Run a hook in each currently open buffer that any fileset applies to."
   ;;$$WRITE ME
   ;;$$FACTOR ME Several parts: Running something in all relevant
   ;;buffers (parms: fileset-name).  The hook.  Running the hook this
   ;;way. 
   ;;$$DESIGN ABOUT ME How and when to trigger this.  There are no
   ;;obvious hooks in fileset. 
   (interactive)
   (let*
      ()
      ;;For each fileset
      ;;Get list of files
      ;;For each one that's already open, with that buffer current,
      ;;run hooks.  Default hook will just set `fileset-whole-name-here'.
      ))
;;;_  . fileset-whole-init
;;;###autoload
(defun fileset-whole-init ()
   "Set up fileset-whole on top of filesets"
   (filesets-init)
   (fileset-whole-setup-menu)
   (fileset-whole-cmds-to-menu))
;;;_ , Structuring
;;;_  . fileset-whole-cmd->fn
(defun fileset-whole-cmd->fn (cmd)
   "Return the function field of CMD"
   (nth 1 cmd))
;;;_  . fileset-whole-cmd->args
(defun fileset-whole-cmd->args (cmd)
   "Return the args field of CMD"
   (nth 2 cmd))
;;;_  . fileset-whole-entry->alist
(defun fileset-whole-entry->alist (entry)
   "Return the alist of ENTRY"
   (cdr entry))
;;;_  . fileset-whole-data-get
(defun fileset-whole-data-get (entry key &optional default carp)
  "Extract the value for KEY in the data part of fileset ENTRY.
Return DEFAULT if not found.  Return (car VALUE) if CARP is non-nil."
  (filesets-alist-get 
     (fileset-whole-entry->alist entry) key default carp))

;;;_ , Running commands
;;;_  . fileset-whole-read-cmd 
(defun fileset-whole-read-cmd ()
   "Interactively get the name of a fileset command.
Either a whole command or a command on individual files."
   
   (completing-read "Select command: " 
      (append filesets-commands fileset-whole-commands)
      nil t))
;;;_  . fileset-whole-read-fileset 
(defun fileset-whole-read-fileset (&optional ask)
   "Interactively get the name of a fileset.
Called interactively, it's useful for changing a buffer's default
   fileset"
   (interactive (list t))
   (or
      (unless ask fileset-whole-name-here)
      (let
	 ((name
	     (completing-read "Select fileset: "
		filesets-data nil t
		nil nil fileset-whole-name-here)))
	 ;;Record it so we won't have to ask again wrt this buffer
	 ;;(`fileset-whole-name-here' is buffer-local)

	 ;;$$IMPROVE ME Allow some way(s) to not set it:
	 ;;User preference, and a buffer-local variable.
	 (unless (eq name fileset-whole-name-here)
	    (setq fileset-whole-name-here name))
	 ;;$$IMPROVE ME If buffer is not associated with
	 ;;any file, ask to add it (consult user preference)
	 ;;`filesets-add-buffer'
	 name)))
;;;_  . fileset-whole-set-fileset
(defalias 'fileset-whole-set-fileset 'fileset-whole-read-fileset)
;;;_  . fileset-whole-get-dir
(defun fileset-whole-get-dir (fileset-whole fileset)
   "Return the root dir of FILESET-WHOLE if it exists."
   ;;$$IMPROVE ME  Store a :root-dir property if we made one.
   (or
      (fileset-whole-data-get fileset-whole :root-dir nil t)
      (fileset-whole-root-dir fileset)))


;;;_  . fileset-whole-run-cmd 
;;;###autoload
(defun fileset-whole-run-cmd (cmd-name &optional fileset-name)
   "Run command CMD-NAME on whole fileset FILESET-NAME.
If prefix arg is given, don't use FILESET-NAME default."
   (interactive
      (list
	 (fileset-whole-read-cmd)
	 (fileset-whole-read-fileset current-prefix-arg)))
   (let
      ((cmd-on-whole (assoc cmd-name fileset-whole-commands)))
      (cond
	 (cmd-on-whole
	    (let*
	       (
		  (fileset 
		     (filesets-get-fileset-from-name fileset-name))
		  (fileset-whole
		     (assoc fileset-name fileset-whole-alist))
		  (default-directory
		     (fileset-whole-get-dir fileset-whole fileset))
		  (fn
		     (fileset-whole-cmd->fn cmd-on-whole))
		  (args-spec
		     (fileset-whole-cmd->args cmd-on-whole))
		  (args
		     (mapcar #'eval args-spec)))
	       
	       ;;$$IMPROVE ME Maybe open the fileset first, as
	       ;;filesets-run-cmd does.  Do this according to some
	       ;;property of the command. `filesets-open'.

	       ;;$$IMPROVE ME When command makes another buffer
	       ;;(if we can tell it did), associate that buffer
	       ;;with FILESET.  Set `fileset-whole-name-here' in
	       ;;it.  Add it as an associated buffer (Possibly
	       ;;in another menu area).
	       (apply fn args)))
	       
	 ;;If not, call the filesets command (which presumably
	 ;;exists since we found it earlier)
	 (t
	    (filesets-run-cmd cmd-name fileset-name)))))

;;;_ , Fileset extras
;;;_  . fileset-whole->fileset
(defun fileset-whole->fileset (fileset)
   "Return a fileset object corresponding to FILESET.
If FILESET is a string, return the fileset of that name."
   
   (cond
      ((stringp fileset)
	 (filesets-get-fileset-from-name fileset))
      ((listp fileset)
	 fileset)
      (t
	 (error "Can't convert to fileset: %s" fileset))))
;;;_  . fileset-whole-get-filelist

;;This may be a bugfix towards filesets, because
;;`filesets-get-filelist' fails on :pattern or :tree.  It's also an
;;extension, to allow multiple :files-type clauses.
(defun fileset-whole-get-filelist (fileset)
   "Return a list of all files in FILESET.
Use instead of `filesets-get-filelist'."
   
   (let*
      (
	 (entries (cdr fileset))
	 (files
	    (apply #'nconc
	       (mapcar
		  #'(lambda (entry)
		       (mapcar
			  #'expand-file-name
			  (case (car entry)
			     ((:pattern :tree :ingroup)
				(filesets-get-filelist
				   (cdr entry) (car entry) nil))
			     (:files (cdr entry))
			     (:file
				(list (cdr entry)))
			     ;;Anything else: nil
			     (t ())
			     )))
		  entries))))
      files))


;;;_  . fileset-whole-pick-file
;;This is suitable as a destination-file function for
;;`org-remember-templates'
(defun fileset-whole-pick-file (&optional fileset-name filter)
   "Interactively pick a single file name from a fileset.
If FILESET-NAME if not given, prompt for it.

If FILTER is given, it must be a function of 1 arg.  Only present
files that satisfy it."
   
   (interactive)
   (let*
      ((fileset-name 
	  (or 
	     fileset-name 
	     (fileset-whole-read-fileset t)))
	 (fileset
	    (filesets-get-fileset-from-name fileset-name))
	 (files
	    (fileset-whole-get-filelist fileset))
	 (files
	    ;;$$IMPROVE ME  Support filtering by extension, by
	    ;;mode-symbol, or by regexp.
	    (if filter
	       (filesets-filter-list files filter)
	       files)))
      
      (completing-read "File: " files nil t
	 (fileset-whole-root-dir fileset))))


;;;_  . fileset-whole-root-dir
(defun fileset-whole-root-dir (fileset)
   "Return a fileset's smallest enclosing directory."
   
   (let*
      ((fileset (fileset-whole->fileset fileset))
	 (files (fileset-whole-get-filelist fileset))
	 (prefix
	    (if files
	       (reduce
		  #'fill-common-string-prefix
		  files)
	       (error "No files found for %s" fileset))))
      (file-name-directory prefix)))



;;;_ , Maintaining fileset-whole data
;;;_  . fileset-whole-edit
(defun fileset-whole-edit ()
   "Customize `fileset-whole-alist'."
   (interactive)
   ;;$$IMPROVE ME Populate it first from fileset-data
   (customize-variable 'fileset-whole-alist))

;;;_  . fileset-whole-populate-alist
;;YAGNI
;;Populate `fileset-whole-alist' with fileset names and whatever data
;;can be deduced about them, such as :root-dir
;;;_ , Support
;;;_  . fileset-whole-apply-compile
(defun fileset-whole-apply-compile (&rest args)
   "Like `compile', but applied to the concatenation of ARGS"
   (compile 
      (mapconcat #'identity args " ")))
;;;_ , To set up bindings
;;;###autoload (global-set-key "\C-cp" #'fileset-whole-run-cmd)

;;;_ , To set up menus
(fileset-whole-init)

;;;_. Footers
;;;_ , Provides

(provide 'fileset-whole)

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

;;;_ , End
;;; fileset-whole.el ends here

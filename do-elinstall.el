;;;_ personal/do-elinstall.el --- Script to do installation

(elinstall
   "fileset-whole"
   (elinstall-directory-true-name)
   '(all
       (dir "./")))

;;;_. Footers
;;;_ , Provides

;;Nothing, this is a script.

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + no-byte-compile: t
;;;_  + no-update-autoloads: t
;;;_  + End:

;;;_ , End
;;; personal/do-elinstall.el ends here
